dns-lexicon>=3.2.1
setuptools>=39.0.1
zope.interface
acme>=1.18.0
certbot>=1.18.0

[docs]
Sphinx>=1.0
sphinx_rtd_theme
